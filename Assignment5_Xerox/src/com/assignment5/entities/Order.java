/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Order {
    
    int orderId;
    int salesId;
    String marketSegment;
    int customerId;
    List<Item> items;

    public Order(int orderId, int salesId, int customerId,String marketSegment) {
        this.orderId = orderId;
        this.salesId = salesId;
        this.customerId = customerId;
        this.marketSegment = marketSegment;
        this.items = new ArrayList<>();
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public String getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }


    public int getAllOrdersTotal(List<Item> item, List<Product> p) {
        int sum = 0;
        for (int i = 0; i < item.size(); i++) {

            sum = sum + item.get(i).getOrderRevenue(p);

        }
        return sum;
    }

    @Override
    public String toString() {
        return "Order{" + "id = " + orderId + ", Sales Id = " + salesId + ", CustomerId = " + customerId + ", ItemListSize = " + items.size() +", market name = "+marketSegment+ '}';
    }
    
    

    
}
