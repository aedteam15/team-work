/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Customer {
    int customerId;
    List<String> marketSegments;
    List<Order> orders;
    
    public Customer(int customerId,List<String> marketSegments){
        this.customerId=customerId;
        this.marketSegments = marketSegments;
        this.orders=new ArrayList<>();
    }

    public List<String> getMarketSegments() {
        return marketSegments;
    }

    public void setMarketSegments(List<String> marketSegments) {
        this.marketSegments = marketSegments;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /*public String getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }*/
    
    @Override
    public String toString(){
        return "Customer Id :"+customerId+" market segment :"+marketSegments;
    }
    
    
    
}
