/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Market;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Kaviya
 */
public class AnalysisHelper {
    
    public void printdata(){
        
    Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
    Map<Integer, Item> items = DataStore.getInstance().getItems();
    Map<Integer, Order> orders = DataStore.getInstance().getOrders();
    Map<Integer, Product> products = DataStore.getInstance().getProducts();
    Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPersons();
    Map<String, Market> markets = DataStore.getInstance().getMarkets();
    
       List<Map.Entry<String, Market>> marketList = new LinkedList<Map.Entry<String, Market>>(markets.entrySet());
        
       // HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
       for (Map.Entry<String, Market> en : marketList) {
            System.out.println(" Value: " + en.getValue());

        }

    
    }
    
    
    // Top 3 best salesperson
    public void topSalesPerson() {

        Map<Integer, Integer> bestProduct = new HashMap<Integer, Integer>();
        
        Map<Integer, Product> prod = DataStore.getInstance().getProducts();
        
        Map<Integer, SalesPerson> salePerson = DataStore.getInstance().getSalesPersons();
        List<Product> p = new ArrayList<>(prod.values());

        for (SalesPerson sp : salePerson.values()) {
            int total = 0;
            for (Order o : sp.getOrders()) {
                int OrderTotal = 0;
                for (Item i : o.getItems()) {
                    if (bestProduct.containsKey(sp.getSalesid())) {
                        total = bestProduct.get(sp.getSalesid());
                    }

                    OrderTotal = o.getAllOrdersTotal(o.getItems(), p);
                }

                total += OrderTotal;
                bestProduct.put(sp.getSalesid(), total);
            }
        }
        List<Map.Entry<Integer, Integer>> list
                = new LinkedList<>(bestProduct.entrySet());

        //Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {

                return o2.getValue() - o1.getValue();
            }
        });

        System.out.println("Top 3 Best Sales People: \n");
        for (int i = 0; i < list.size() && i < 3; i++) {
            System.out.println("sales person with id "+salePerson.get(list.get(i).getKey()).getSalesid() + ", has generated Revenue of " + list.get(i).getValue());
        }
    }
    
        public void totalRevenueBySegment(){
        
 
        Map<Integer,Product> prod = DataStore.getInstance().getProducts();
        
        Map<String, Market> markets = DataStore.getInstance().getMarkets();
        Map<Market,Integer> marketRevenue = new HashMap<>();
        List<Product> p = new ArrayList<>(prod.values());
        
        for (Market market : markets.values()) {
            int total = 0;
            for (Order o : market.getOrders()) {
                int OrderTotal = o.getAllOrdersTotal(o.getItems(), p);
                total += OrderTotal;
                }
                
                marketRevenue.put(market, total);
            }
        
        List<Map.Entry<Market, Integer>> marketList
                = new LinkedList<>(marketRevenue.entrySet());

        for(int i=0; i<marketList.size(); i++){
            System.out.println(marketList.get(i).getKey().getMarketSegement()+" segment has generated revenue of "+marketList.get(i).getValue()  );
        }  
    }
    
   /*
    *Our top 5 most popular product sorted from high to low.
    */
    public void topFiveProduct(){
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Integer> productOrderCount = new LinkedHashMap<>();
        List<Map.Entry<Integer, Integer>> inactiveUserList ;
        for(Order order:orders.values()){
            List<Item> items= order.getItems();
            for(Item item: items){ 
                int totalCustomerOrderPrice=0;
                Product product = products.get(item.getProductId());
                        if(productOrderCount.containsKey(product.getProductId())){
                            totalCustomerOrderPrice = productOrderCount.get(product.getProductId());                     
                         }
                        totalCustomerOrderPrice += item.getQuantity()*(item.getSalesPrice()-product.getMinPrice());
                        productOrderCount.put(product.getProductId(), totalCustomerOrderPrice);                      
                }
            }
        inactiveUserList = new LinkedList<Map.Entry<Integer, Integer>>(productOrderCount.entrySet());
         Collections.sort(inactiveUserList, new Comparator<Map.Entry<Integer, Integer>>() {
           @Override
           public int compare(Map.Entry<Integer, Integer> o1,
                   Map.Entry<Integer, Integer> o2) {
               return (o2.getValue()).compareTo(o1.getValue());
           }
       });
       
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : inactiveUserList) {
           temp.put(aa.getKey(), aa.getValue());
        }

       System.out.println("Top Five Products: ");
       int counter = 1;
       for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
           System.out.println("Product Id : " + en.getKey()
                   + ", total rev of the product : " + en.getValue());
           counter++;

            if (counter == 6) {
                break;
            }
        }                              
    }
   /*
    *To find the top customers in certain markets 
    */
    public void topCustomersInMarket(){
        Map<String, Market> markets = DataStore.getInstance().getMarkets();
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        List<Map.Entry<Integer, Integer>> orderByTotalSales ;
        Map<Integer,Integer> totalCustomerOrderSalesPrice;       
        List<Product> productList = new ArrayList<>(products.values());
        System.out.println("Top Customers in each Market Segmetn");
        for(Market market :markets.values()){
            totalCustomerOrderSalesPrice = new HashMap<>();
            for(Integer customerID: market.getCustomerIds())
            {
               Customer customer = customers.get(customerID);          
                int totalCustomerOrderPrice = 0;
                for(Order order:customer.getOrders()){
                    if(order.getMarketSegment().equalsIgnoreCase(market.getMarketSegement())){
                        int OrderTotal = order.getAllOrdersTotal(order.getItems(), productList);
                        totalCustomerOrderPrice += OrderTotal;           
                        totalCustomerOrderSalesPrice.put(customer.getCustomerId(), totalCustomerOrderPrice);
                    }           
                }
            }

           orderByTotalSales = new LinkedList<Map.Entry<Integer, Integer>>(totalCustomerOrderSalesPrice.entrySet());
           Collections.sort(orderByTotalSales, new Comparator<Map.Entry<Integer, Integer>>() {
               @Override
               public int compare(Map.Entry<Integer, Integer> o1,
                       Map.Entry<Integer, Integer> o2) {
                   return (o2.getValue()).compareTo(o1.getValue());
               }
           });

            HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
            for (Map.Entry<Integer, Integer> aa : orderByTotalSales) {
               temp.put(aa.getKey(), aa.getValue());
           }

           System.out.println("Market Segment:"+ market.getMarketSegement());
           int counter = 1;
           for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
               System.out.println("Cutomer Id : " + en.getKey()
                       + ", total revenue : " + en.getValue()); 
               counter++;
               if(counter==6){
                   break;
               }
            }                              
        }
    }
      //****Total Revenue by year*****
         public void totRevenue(){
        
 
        Map<Integer,Product> prod = DataStore.getInstance().getProducts();
        
        Map<String, Market> markets = DataStore.getInstance().getMarkets();
        Map<Market,Integer> marketRevenue = new HashMap<>();
        List<Product> p = new ArrayList<>(prod.values());
        
        for (Market market : markets.values()) {
            int total = 0;
            for (Order o : market.getOrders()) {
                int OrderTotal = o.getAllOrdersTotal(o.getItems(), p);
                total += OrderTotal;
                }
                
                marketRevenue.put(market, total);
            }
        
        List<Map.Entry<Market, Integer>> marketList
                = new LinkedList<>(marketRevenue.entrySet());
        
        int totalRevenue=0;
        for(int i=0; i<marketList.size(); i++){
                        totalRevenue+=marketList.get(i).getValue();
        }
        System.out.println("Total Revenue:"+totalRevenue);    
        }
       
        
     //  5 best custome
    public void bestCustomer() {
        
          Map<Integer, Integer> bestProduct = new HashMap<Integer, Integer>();
        Map<Integer, Product> prod = DataStore.getInstance().getProducts();

        Map<Integer, Order> order = DataStore.getInstance().getOrders();
        
        Map<Integer, Customer> cust = DataStore.getInstance().getCustomers();
        List<Product> p = new ArrayList<>(prod.values());

        for (Customer c : cust.values()) {
            int total = 0;
            for (Order o : c.getOrders()) {
                int OrderTotal = 0;
                for (Item i : o.getItems()) {
                    if (bestProduct.containsKey(c.getCustomerId())) {
                        total = bestProduct.get(c.getCustomerId());
                    }

                    OrderTotal = o.getAllOrdersTotal(o.getItems(), p);
                }

                total += OrderTotal;
                bestProduct.put(c.getCustomerId(), total);
            }
        }
        List<Map.Entry<Integer, Integer>> list
                = new LinkedList<>(bestProduct.entrySet());

        //Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {

                return o2.getValue() - o1.getValue();
            }
        });

        System.out.println("Top 5 Best Customer: \n");
        for (int i = 0; i < list.size() && i <5; i++) {
            System.out.println("Customer with id "+cust.get(list.get(i).getKey()).getCustomerId() + ", has generated Revenue of " + list.get(i).getValue());
        }
    }
   
}

