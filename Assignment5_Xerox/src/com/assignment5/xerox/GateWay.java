/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Market;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import com.assignment5.entities.analytics.AnalysisHelper;
import com.assignment5.entities.analytics.DataStore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kasai
 */
public class GateWay {
        DataReader productReader;
        DataReader orderReader ;
        AnalysisHelper helper;
        public GateWay() throws IOException {    
        DataGenerator generator = DataGenerator.getInstance();
        productReader = new DataReader(generator.getProductCataloguePath());
        orderReader = new DataReader(generator.getOrderFilePath());
        helper = new AnalysisHelper();
        } 
        
        public static void main(String args[]) throws IOException{
        GateWay inst = new GateWay();
        inst.readData();
        }
        private void readData() throws IOException{
        String[] row;
        Product prod = null ; 
        System.out.println("Inside ReadData") ;
        while((row = productReader.getNextRow()) != null ){
                prod = generateProduct(row);          
        }
        while((row = orderReader.getNextRow()) != null ){
           
            if(prod != null){
                
                Item item = generateItem(row, prod);
                //System.out.println(item); 
                Order order = generateOrder(row, item);
               // System.out.println(order);
                if(order!=null){
                
                generateCustomer(row,order);
                generateSalesPerson(row,order);
                generateMarket(row,order);
                }
            
            }else {
            
            
            System.out.println("Product/Order is null !!");
            }
        }
        
       runAnalysis();
    }
        
    
   private Product generateProduct(String[] row){
       int prodId = 0;
       int minPrice = 0 ;
       int maxPrice = 0 ;
       int targetPrice = 0 ;
       
       try {
            prodId = Integer.parseInt(row[0]);
            minPrice = Integer.parseInt(row[1]);
            maxPrice = Integer.parseInt(row[2]);
            targetPrice = Integer.parseInt(row[3]);
        }catch(Exception e) {
        
           System.out.println("Generate Product : Error while generating Product data from csv file.") ;
        }

       Product prod = new Product(prodId, minPrice, maxPrice,targetPrice);
        DataStore.getInstance().getProducts().put(prodId,prod);
        return prod ;
    }
        
    
    private Item generateItem(String[] row,Product prod){
    
    int itemId =0 ;
    int productId =0 ;
    int salesPrice =0 ;
    int quantity= 0;
    
    try {
            itemId = Integer.parseInt(row[1]);
            productId = Integer.parseInt(row[2]);
            salesPrice = Integer.parseInt(row[6]);
            quantity = Integer.parseInt(row[3]);
            
        }catch(Exception e) {
        
           System.out.println("Generate Item : Error while generating Item data from csv file.") ;
        }
      Item item = new Item(itemId,productId, salesPrice, quantity);
      DataStore.getInstance().getItems().put(itemId,item);
      return item ;
    
    }
    
    private Order generateOrder(String[] row,Item item){
        
    int orderId=0;
    int salesId=0;
    int customerId=0;
    String marketSegment = row[7];

    Order order = null ;
   
    try {
            orderId = Integer.parseInt(row[0]);
            salesId = Integer.parseInt(row[4]);
            customerId = Integer.parseInt(row[5]);
            
        }catch(Exception e) {
        
           System.out.println("Generate Order : Error while generating Order data from csv file.") ;
        }
        
       Map<Integer,Order> Order = DataStore.getInstance().getOrders();
       if (Order.containsKey(item.getItemId()))
        Order.get(item.getItemId()).getItems().add(item);
       else {
       order = new Order(orderId,salesId,customerId,marketSegment) ;
       order.getItems().add(item);
       Order.put(orderId,order);
       
      }
       return order ;
    }   
       
       private void generateCustomer(String[] row ,Order order) {
       List<String> marketSegmentList = new ArrayList<>();
           int id =0 ;
           String marketSegment=row[7];
           try {
            id = Integer.parseInt(row[5]);
            
        }catch(Exception e) {
        
           System.out.println("Generate Customer : Error while generating Customer data from csv file.") ;
        }
        Map<Integer,Customer> customers = DataStore.getInstance().getCustomers();
       if(customers.containsKey(order.getCustomerId())){
          customers.get(order.getCustomerId()).getOrders().add(order);
           customers.get(order.getCustomerId()).getMarketSegments().add(marketSegment);
           
       }else {
           
           marketSegmentList.add(marketSegment);
       Customer cust = new Customer (id,marketSegmentList);
       cust.getOrders().add(order);
       customers.put(id,cust);
       
       
       
       }
       }
       
       
    private void generateSalesPerson(String[] row ,Order order) {
     
           int id =0 ;
           try {
            id = Integer.parseInt(row[4]);
            
        }catch(Exception e) {
        
           System.out.println("Generate Customer : Error while generating Customer data from csv file.") ;
        }
        Map<Integer,SalesPerson> salePerson = DataStore.getInstance().getSalesPersons();
       if(salePerson.containsKey(order.getSalesId())){
         salePerson.get(order.getSalesId()).getOrders().add(order);  
          
       }else {
        SalesPerson sp = new SalesPerson(id);
        sp.getOrders().add(order);
        salePerson.put(id,sp);
       
       
       }
    }
    
    private void generateMarket(String[] row ,Order order){
        int id=0;
        String marketSegment=row[7];
        List<Integer> customerIds = new ArrayList<>();
        try{
           id = Integer.parseInt(row[5]);
        }
        catch(Exception e) {
        
           System.out.println("Generate Market : Error while generating Market data from csv file.") ;
        }
         Map<String, Market> markets = DataStore.getInstance().getMarkets();
       if(markets.containsKey(order.getMarketSegment())){
           
          markets.get(order.getMarketSegment()).getOrders().add(order);
          markets.get(order.getMarketSegment()).getCustomerIds().add(id);
           
           
       }else {
       Market market = new Market (id,marketSegment);
       market.getOrders().add(order);
       market.getCustomerIds().add(id);
       markets.put(marketSegment,market);
       
       
       
       }       
        
    }
     
    private void runAnalysis(){
        helper.topSalesPerson();
        System.out.println("========================================================================================");
        helper.totalRevenueBySegment();
         System.out.println("========================================================================================");
        helper.topFiveProduct();
        System.out.println("==========================================================================================");
        helper.topCustomersInMarket();       
        System.out.println("============================Total Revnue================================================");
        helper.totRevenue();
        System.out.println("============================Best Customer================================================");
        helper.bestCustomer();
           
       
    }
    

}
    

    

