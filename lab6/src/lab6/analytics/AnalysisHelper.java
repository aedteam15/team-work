/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lab6.entities.Comment;
import lab6.entities.Post;
import lab6.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    //  key: UserId ; Value: sum of likes from all comments
    public void userWithMostLikes() {
        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
    
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikesCount.containsKey(user.getId())) {
                    likes = userLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for (int id : userLikesCount.keySet()) {
            if (userLikesCount.get(id) > max) {
                max = userLikesCount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most likes: " + max + "\n" 
            + users.get(maxId));
    }
    
    
    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>() {
            @Override 
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });
        
        System.out.println("5 most likes comments: ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }
    }

    public void postwithmostlikedcomment(){

       Map<Integer, Integer> postLikesCount = new HashMap<>();

       Map<Integer, Post> posts = DataStore.getInstance().getPosts();

       for (Post post : posts.values()) {
           for (Comment c : post.getComments()) {
               int likes = 0;
               if (postLikesCount.containsKey(post.getPostId())) {
                   likes = postLikesCount.get(post.getPostId());
               }
               likes += c.getLikes();
               postLikesCount.put(post.getPostId(), likes);
           }
   }
       int max = 0;
       int maxId = 0;
       for (int id : postLikesCount.keySet()) {
           if (postLikesCount.get(id) > max) {
               max = postLikesCount.get(id);
               maxId = id;
           }
       }
       System.out.println("Post with most likes: " + max + "\n"
         + posts.get(maxId));
}

    // find post with Most Comments
   //  key: PostId ; Value: sum of likes from all comments
    public void postwithmostcomment(){


       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       List<Post> mostComment = new ArrayList<>(posts.values());

       Collections.sort(mostComment, new Comparator<Post>() {
           @Override
           public int compare(Post o1, Post o2) {
               return o2.getComments().size() - o1.getComments().size();

           }
       });

      System.out.println("Post with most comments: "+mostComment.get(0));

   }
    
    

  public void avgLikesPerComment(){
      ArrayList<Comment> totalComments = new ArrayList<>();
      ArrayList<Integer> likes= new ArrayList<>();
      float sumOflikes=0;
      Map<Integer, Post> posts = DataStore.getInstance().getPosts();
      for(Post post:posts.values()){
          for (Comment comment:post.getComments()){
              totalComments.add(comment);
              sumOflikes = sumOflikes+comment.getLikes();
              
          }
      }
      int noComments = totalComments.size();
      System.out.println("Average likes per comment : "+ (sumOflikes/totalComments.size()));
      

  }
  public void top5inactiveUsers(){
      
      Map<Integer, User> users = DataStore.getInstance().getUsers();
      Map<Integer, Post> posts = DataStore.getInstance().getPosts();
      Map<Integer, Integer> userActivity = new HashMap<>();
      //Map<Integer, Integer> postActivity = new HashMap<>();

      
      for(User user: users.values()){
          int sumOfLikes = 0;
          int sumOfPosts =0;
          for(Post post:posts.values()){
              if(post.getUserId()==user.getId()){
                  sumOfPosts++;
              for(Comment comment: post.getComments()){
                  int noOfComments = post.getComments().size();
                  sumOfLikes = sumOfLikes+comment.getLikes();
                  userActivity.put(user.getId(), noOfComments+sumOfLikes+sumOfLikes);
                 }
              }
              
          }
      }
      
      //Collections.sort(list);
       List<Map.Entry<Integer, Integer>> proactiveUserList = new LinkedList<Map.Entry<Integer, Integer>>(userActivity.entrySet());
        Collections.sort(proactiveUserList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1,
                    Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : proactiveUserList) {
            temp.put(aa.getKey(), aa.getValue());
        }

        System.out.println("5 most inactive users overall: ");
        int counter = 1;
        for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
            System.out.println("User Id : " + en.getKey()
                    + ", sum of Likes,Comments and Posts : " + en.getValue());
            counter++;

            if (counter == 6) {
                break;
            }
        }
     
      
  }
  public void top5ProactiveUsers(){
      
      Map<Integer, User> users = DataStore.getInstance().getUsers();
      Map<Integer, Post> posts = DataStore.getInstance().getPosts();
      Map<Integer, Integer> userActivity = new HashMap<>();
      //Map<Integer, Integer> postActivity = new HashMap<>();

      
      for(User user: users.values()){
          int sumOfLikes = 0;
          int sumOfPosts =0;
          for(Post post:posts.values()){
              if(post.getUserId()==user.getId()){
                  sumOfPosts++;
              for(Comment comment: post.getComments()){
                  int noOfComments = post.getComments().size();
                  sumOfLikes = sumOfLikes+comment.getLikes();
                  userActivity.put(user.getId(), noOfComments+sumOfLikes+sumOfLikes);
                 }
              }
              
          }
      }
      
      //Collections.sort(list);
       List<Map.Entry<Integer, Integer>> proactiveUserList = new LinkedList<Map.Entry<Integer, Integer>>(userActivity.entrySet());
        Collections.sort(proactiveUserList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1,
                    Map.Entry<Integer, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : proactiveUserList) {
            temp.put(aa.getKey(), aa.getValue());
        }

        System.out.println("5 most Proactive users overall: ");
        int counter = 1;
        for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
            System.out.println("User Id : " + en.getKey()
                    + ", sum of Likes,Comments and Posts : " + en.getValue());
            counter++;

            if (counter == 6) {
                break;
            }
        }
     
      
  }

 public void getInactiveUsersOnPosts(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> userList = new LinkedHashMap<>();
        
        for (Post post : posts.values()) {
            int counter = 0;           
            if(userList.containsKey(post.getUserId())){
                counter = userList.get(post.getUserId());
            }
            counter +=1;
            userList.put(post.getUserId(), counter);
        }
        Set<Integer> keys = (Set<Integer>) users.keySet();
        for(Integer val: keys){
            if(!userList.containsKey(val)){
                userList.put(val,0);
            }
        }
        List<Map.Entry<Integer, Integer>> proactiveUserList = new LinkedList<Map.Entry<Integer, Integer>>(userList.entrySet());
       Collections.sort(proactiveUserList, new Comparator<Map.Entry<Integer, Integer>>() {
           @Override
           public int compare(Map.Entry<Integer, Integer> o1,
                   Map.Entry<Integer, Integer> o2) {
               return (o1.getValue()).compareTo(o2.getValue());
           }
       });
       
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
       for (Map.Entry<Integer, Integer> aa : proactiveUserList) {
           temp.put(aa.getKey(), aa.getValue());
       }

       System.out.println("5 most Inactive users based on Post: ");
       int counter = 1;
       for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
           System.out.println("User Id : " + en.getKey()
                   + ", sum of Posts : " + en.getValue());
           counter++;

           if (counter == 6) {
               break;
           }
       }   
    }
    
    public void getInactiveUsersOnComments(){
        Map<Integer, Integer> userCommentsCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
    
        
        for (User user : users.values()) {
            userCommentsCount.put(user.getId(),user.getComments().size());
        }
        Set<Integer> keys = (Set<Integer>) users.keySet();
        for(Integer val: keys){
            if(!userCommentsCount.containsKey(val)){
                userCommentsCount.put(val,0);
            }
        }
        
          List<Map.Entry<Integer, Integer>> inactiveUserList = new LinkedList<Map.Entry<Integer, Integer>>(userCommentsCount.entrySet());
       Collections.sort(inactiveUserList, new Comparator<Map.Entry<Integer, Integer>>() {
           @Override
           public int compare(Map.Entry<Integer, Integer> o1,
                   Map.Entry<Integer, Integer> o2) {
               return (o1.getValue()).compareTo(o2.getValue());
           }
       });
       
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
       for (Map.Entry<Integer, Integer> aa : inactiveUserList) {
           temp.put(aa.getKey(), aa.getValue());
       }

       System.out.println("5 most Inactive users based on Comments: ");
       int counter = 1;
       for (Map.Entry<Integer, Integer> en : temp.entrySet()) {
           System.out.println("User Id : " + en.getKey()
                   + ", sum of Comments : " + en.getValue());
           counter++;

           if (counter == 6) {
               break;
           }
       }                
    }
 }
