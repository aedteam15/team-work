/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author navee
 */
public class Market {
    private String marketSegement;
    List<Integer> customerIds;
    List<Order> orders;

    public Market(int customerId,String marketSegement) {
        this.marketSegement = marketSegement;
        this.customerIds = new ArrayList<>();
        this.orders =new ArrayList<>();
        
    }

    public String getMarketSegement() {
        return marketSegement;
    }

    public void setMarketSegement(String marketSegement) {
        this.marketSegement = marketSegement;
    }

    public List<Integer> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Integer> customerIds) {
        this.customerIds = customerIds;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
    
//    public int getRevenueOnMarket(){
//        int totalRevenue=0;
//        List<Product> products = new ArrayList<>();
//        
//        for(Order order:orders){
//            totalRevenue+=order.getAllOrdersTotal(order.getItems(), )
//        }
//        
//        return totalRevenue;
//    }
    
    @Override
    public String toString(){
        return "In Market: "+marketSegement+" there are "+customerIds.size()+" present and total orders are "+orders.size();
   }
    
    
    
    
    
}
